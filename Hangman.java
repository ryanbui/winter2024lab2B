/* 
* assignment 2 2023 - 110
* 
* program  to create a Hangman game 
* using Strings, methods, chars, variables, 
* input from the user and output
* @author Ryan Bui 2331822
* @version 2023-11
*/ 

import java.util.Scanner;

/*
* The main method will use a Scanner to request a 4 letter word with different letters from Player 1.
* It will then call the void method runGame() with Player 1's word and start the Hangman Game. 
*/
public class Hangman{

	/*
	* This method should take as parameters a String representing a word, and a char 
	* containing a letter we would like to find in that word. With the String method 
	* charAt() and a for loop, the method should return the position at which the 
	* character exists in the word (0-3), or -1 if it does not exist in the word.
	*/
	public static int isLetterInWord(String word, char letter){
		letter = toUpperCase(letter);
		for(int i = 0; i < word.length(); i++){
			if(toUpperCase(word.charAt(i)) == letter){
				return i;
			}
		} return -1;
	}	
	
	/*
	* This method should take as input a character, and should return that character 
	* in uppercase, regardless of the case of the original character. This will ensure the 
	* game is case insensitive, so that it doesn’t matter whether the input was uppercase or lowercase.
	*/
	public static char toUpperCase(char letter){
		if(letter >= 'a' && letter <= 'z'){
			return letter -= 32;
		} else {
			return letter;
		}
	}
	
	/*
	* This method should take as parameters a string representing a word, and 4 boolean variables 
	* in an array representing the letters being guessed. With the String method charAt(), the method should 
	* display the letters of the word when the boolean variable is true, meaning the letters were
	* guessed correctly, and an underscore if false.
	*/
	public static void printWork(String word, boolean [] letterStatus){
		char [] letter = new char [word.length()];
		for(int i = 0; i < word.length(); i++){
			if(letterStatus[i]){
				letter[i] = word.charAt(i);
			} else {
				letter[i] = '_';
			}
		} 
		System.out.println("Your result is " + letter[0] + letter[1] + letter[2] + letter[3]);
	}

	/*
	* This method should take as input a String representing the word Player 2 is trying to guess. 
	* 4 booleans in an array representing the positions of each of the letters in the word being guessed.
	* At the start, they should all be set to false, but as Player 2 guessed the correct letters they should be set to true.
	* A loop which continously asks Player 2 to enter a letter, which will call the method isLetterInWord.
	* The loop should continue until all booleans are set to true, meaning Player 2 wins, or until they make 6 misses.
	* If the method isLetterInWord returns a positive position, the boolean is set to true, and if negative, number of misses increases by 1.
	* After every guess, display current state of the game by calling the method printWork with the word and the array of booleans.
	*/
	public static void runGame(String word){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		boolean [] positionCorrect  = {false, false, false, false};
		
		int missCounter = 0;
		boolean gameWon = false;
		while(missCounter < 6 && !(gameWon)){
			if(positionCorrect[0] && positionCorrect[1] && positionCorrect[2] && positionCorrect[3]){
				gameWon = true;
				System.out.println("You win!");
			} else {
				System.out.println("Player 2 guess a letter");
				char letter = reader.next().charAt(0);
				int i = isLetterInWord(word,letter);
				if(i >=0 && i <= 3){
					positionCorrect[i] = true;
				} else {
					missCounter++;
				}
				printWork(word,positionCorrect);
				System.out.println("Miss Counter: " + missCounter);
			}
			if(missCounter == 6){
				System.out.println("Better luck next time.");
			}		
		} 
	}
}
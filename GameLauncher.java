import java.util.Scanner;

public class GameLauncher{
	public static void main(String [] args){
		Scanner reader = new java.util.Scanner(System.in);
		playGame();
		
		boolean playAgain = true;
		
		while(playAgain){
			System.out.println("Would you like to keep playing? Enter 1 to keep playing or 2 to stop playing");
			int answer = reader.nextInt();
			
			while(!(answer == 1 || answer == 2)){
			System.out.println("Invalid! Please enter 1 or 2 to choose your option");
			answer = reader.nextInt();
			}	
			if(answer == 1){
				playGame();
			}else{
				playAgain = false;
			}
		}
	}
	public static void playGame(){
		Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Hello! What game would you like to play? Enter 1 to play Hangman, or 2 to play Wordle.");
		int gameChoice = reader.nextInt();
		
		while(!(gameChoice == 1 || gameChoice == 2)){
			System.out.println("Invalid! Please enter 1 or 2 to choose your game");
			gameChoice = reader.nextInt();
		}
		if(gameChoice == 1){
			System.out.println("Player 1 please enter a 4 letter word with different letters to start the Hangman game.");
			String player1Word = reader.next();
			Hangman.runGame(player1Word);
		} else if(gameChoice == 2){
			String gameWord = Wordle.generateWord();
			Wordle.runGame(gameWord);
		}
	}
}
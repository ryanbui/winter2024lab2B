/*
*assignment 3 2023 - 110
*program  to create a Wordle game
*using all elements of the course
*including arrays, methods, loops, Strings
*input from the user and output
*@author Ryan Bui 2331822
*@version 2023-12
*/ 

import java.util.Random;
import java.util.Scanner;

public class Wordle {
     
    /*
    * Method to pick a random word as the target word
    * from an array of 20 5-letter words 
    * return the target word
    */
    public static String generateWord(){
        String [] wordList = {"JOKER", "SMOKE", "JUICE", "GRAPE", "QUICK", 
        "ENZYM", "QUACK", "BLINK", "CRAZY", "FIELD", "PRIZE", "CHOKE", "HEAVY", 
        "THICK", "BLACK", "WHITE", "CHIEF", "FANCY", "STRAY", "FLASH"};

        Random random = new Random();
        String randomWord = wordList[random.nextInt(wordList.length)];

        return randomWord;
    }

    /*
    * Method to determine if the letter is in the word 
    * given a word and a letter
    * return true if it is, false if it isn't
    */
    public static boolean isLetterInWord(String word, char letter){
        for(int i = 0; i < word.length(); i++){
            if(word.charAt(i) == letter){
                return true;
            }
        } return false;
    }

    /*
    * Method to determine if the letter is in the word at a specified location
    * given a word, a letter, and the position that letter should appear at in the word
    * return true if it is, false if it isn't or the location is out of range. 
    */
    public static boolean letterInSlot(String word, char letter, int position){
        if(position > word.length()){
            return false;
        }
        if(isLetterInWord(word, letter) && word.charAt(position) == letter){
            return true;
        }
        return false;
    }

    /*
    * Method to compare two Strings
    * the first being the word the user is trying to guess (answer)
    * the second being the user's guess
    * use the 2 methods above (letterInWord & letterInSlot)
    * if the letter at a given location is correct, it should be coloured green
    * if the position is wrong, but the letter is in the word, it should be coloured yellow
    * if the letter is not in the word at all, it should be coloured white
    * return an array of the colours where each element in the array represents to
    * a position in the guessed word
    */
    public static String [] guessWord(String answer, String guess){
        String [] colours  = new String [guess.length()];
        for(int i = 0; i < answer.length(); i++){
            if(letterInSlot(answer, guess.charAt(i), i)){
                colours[i] = "green";
            } else if(isLetterInWord(answer, guess.charAt(i))){
                colours[i] = "yellow";
            } else {
                colours[i] = "white";
            }
        }
        return colours;
    }

    /*
    * Method to print each of the letters of word in its corresponding colour 
    * given a word which represents the guessed word and an array of colours
    */
    public static void presentResults(String word, String [] colours){
        for(int i = 0; i < word.length(); i++){
            String letter = Character.toString(word.charAt(i));
            if(colours[i] == "green"){
                System.out.print("\u001B[32m" + letter + "\u001B[0m" + " ");
            } else if(colours[i] == "yellow"){
               System.out.print("\u001B[33m" + letter + "\u001B[0m" + " ");
            } else {
                System.out.print(letter + " ");
            }
        }
        System.out.println();
    }

    /*
    * Method to ask the user to input a guess word
    * if the input is not 5 letters it will ask the user for one again
    */
    public static String readGuess(){
        java.util.Scanner reader = new java.util.Scanner(System.in);
        System.out.println("WORDLE! Please enter a 5-letter word with different letters");
        String guess = reader.next();
        while(!(guess.length() == 5)){
            System.out.println("Invalid! Please enter a 5-letter word with different letters");
            guess = reader.next();
        }
        return guess.toUpperCase();
    }

    /*
    * Method to run the game
    * given a String representing the word the user has to guess (word)
    * Use the readGuess method to ask the user to input a guess
    * Use the guessWord and presentResults method give the user feedback 
    * about what letters were in the right position in their guess
    * Use a loop to set the max amount of attempts to 6 for the user to guess
    * If the user guesses the word correctly, print "You win!"
    * If they exceed the 6 attempts and do not guess it, print "Try again!"
    */
    public static void runGame(String word){
        boolean gameWon = false;
        int numOfGreens = 0;

        for(int numOfAttempts = 0; numOfAttempts < 6 && !(gameWon); numOfAttempts++){
            String guessWord = readGuess();
            String [] resultColours = guessWord(word, guessWord);
            presentResults(guessWord, resultColours);

            for(int j = 0; j < word.length(); j++){
                if(resultColours[j].equals("green")){
                    numOfGreens++;
                }if(numOfGreens == 5){
                gameWon = true;
            }
            }
        }
        if(!(gameWon)){
            System.out.println("Try again!");
        } else if(gameWon){
            System.out.println("You win!");
        }
    }
}